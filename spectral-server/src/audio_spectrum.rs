// This file is generated by rust-protobuf 2.4.0. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq, Clone, Default)]
pub struct AudioSpectrum {
    // message fields
    pub num_bands: i32,
    pub bands: ::std::vec::Vec<f32>,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl AudioSpectrum {
    pub fn new() -> AudioSpectrum {
        ::std::default::Default::default()
    }

    // int32 num_bands = 1;

    pub fn clear_num_bands(&mut self) {
        self.num_bands = 0;
    }

    // Param is passed by value, moved
    pub fn set_num_bands(&mut self, v: i32) {
        self.num_bands = v;
    }

    pub fn get_num_bands(&self) -> i32 {
        self.num_bands
    }

    // repeated float bands = 2;

    pub fn clear_bands(&mut self) {
        self.bands.clear();
    }

    // Param is passed by value, moved
    pub fn set_bands(&mut self, v: ::std::vec::Vec<f32>) {
        self.bands = v;
    }

    // Mutable pointer to the field.
    pub fn mut_bands(&mut self) -> &mut ::std::vec::Vec<f32> {
        &mut self.bands
    }

    // Take field
    pub fn take_bands(&mut self) -> ::std::vec::Vec<f32> {
        ::std::mem::replace(&mut self.bands, ::std::vec::Vec::new())
    }

    pub fn get_bands(&self) -> &[f32] {
        &self.bands
    }
}

impl ::protobuf::Message for AudioSpectrum {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.num_bands = tmp;
                }
                2 => {
                    ::protobuf::rt::read_repeated_float_into(wire_type, is, &mut self.bands)?;
                }
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                }
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if self.num_bands != 0 {
            my_size += ::protobuf::rt::value_size(1, self.num_bands, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += 5 * self.bands.len() as u32;
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if self.num_bands != 0 {
            os.write_int32(1, self.num_bands)?;
        }
        for v in &self.bands {
            os.write_float(2, *v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> AudioSpectrum {
        AudioSpectrum::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "num_bands",
                    |m: &AudioSpectrum| { &m.num_bands },
                    |m: &mut AudioSpectrum| { &mut m.num_bands },
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeFloat>(
                    "bands",
                    |m: &AudioSpectrum| { &m.bands },
                    |m: &mut AudioSpectrum| { &mut m.bands },
                ));
                ::protobuf::reflect::MessageDescriptor::new::<AudioSpectrum>(
                    "AudioSpectrum",
                    fields,
                    file_descriptor_proto(),
                )
            })
        }
    }

    fn default_instance() -> &'static AudioSpectrum {
        static mut instance: ::protobuf::lazy::Lazy<AudioSpectrum> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const AudioSpectrum,
        };
        unsafe {
            instance.get(AudioSpectrum::new)
        }
    }
}

impl ::protobuf::Clear for AudioSpectrum {
    fn clear(&mut self) {
        self.clear_num_bands();
        self.clear_bands();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for AudioSpectrum {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for AudioSpectrum {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x13audioSpectrum.proto\"B\n\rAudioSpectrum\x12\x1b\n\tnum_bands\x18\
    \x01\x20\x01(\x05R\x08numBands\x12\x14\n\x05bands\x18\x02\x20\x03(\x02R\
    \x05bandsJ\xb5\x01\n\x06\x12\x04\0\0\x05\x01\n\x08\n\x01\x0c\x12\x03\0\0\
    \x12\n\n\n\x02\x04\0\x12\x04\x02\0\x05\x01\n\n\n\x03\x04\0\x01\x12\x03\
    \x02\x08\x15\n\x0b\n\x04\x04\0\x02\0\x12\x03\x03\x08\x1c\n\r\n\x05\x04\0\
    \x02\0\x04\x12\x04\x03\x08\x02\x17\n\x0c\n\x05\x04\0\x02\0\x05\x12\x03\
    \x03\x08\r\n\x0c\n\x05\x04\0\x02\0\x01\x12\x03\x03\x0e\x17\n\x0c\n\x05\
    \x04\0\x02\0\x03\x12\x03\x03\x1a\x1b\n\x0b\n\x04\x04\0\x02\x01\x12\x03\
    \x04\x08!\n\x0c\n\x05\x04\0\x02\x01\x04\x12\x03\x04\x08\x10\n\x0c\n\x05\
    \x04\0\x02\x01\x05\x12\x03\x04\x11\x16\n\x0c\n\x05\x04\0\x02\x01\x01\x12\
    \x03\x04\x17\x1c\n\x0c\n\x05\x04\0\x02\x01\x03\x12\x03\x04\x1f\x20b\x06p\
    roto3\
";

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
