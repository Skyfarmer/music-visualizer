extern crate gstreamer as gst;

use std::io;
use std::io::{ErrorKind, Write};
use std::io::Read;
use std::sync::mpsc;
use std::thread;

use protobuf::Message;

use crate::audio_spectrum::AudioSpectrum;
use crate::server_requests::{ServerRequest, ServerRequest_Type};

mod player;
mod audio_spectrum;
mod server_requests;

//TODO UNIT TESTS!!!
fn main() {
    let (t_spectrum, r_spectrum) = mpsc::channel();
    let (t_requests, r_requests) = mpsc::channel();

    //TODO REMOVE UNWRAPS
    thread::spawn(move || {
        let mut stdin = io::stdin();
        let mut stdout = io::stdout();
        loop {
            //TODO Should be possible to solve more nicely
            let size = {
                let mut tmp: usize = 0;
                let mut buffer: [u8; 1] = [255; 1];
                while buffer[0] == 255 {
                    stdin.read_exact(&mut buffer)?;
                    tmp += buffer[0] as usize;
                }
                tmp
            };
            let mut message_buffer = Vec::with_capacity(size);
            stdin.read_exact(&mut message_buffer[0..size])?;

            let message: ServerRequest = protobuf::parse_from_bytes(&message_buffer).unwrap();

            match message.field_type {
                ServerRequest_Type::GET_AUDIO_SPECTRUM => {
                    t_requests.send(message.field_type).unwrap();
                    let msg: AudioSpectrum = match r_spectrum.recv() {
                        Ok(s) => s,
                        Err(e) => return Err(io::Error::new(ErrorKind::Other, e.to_string()))
                    };
                    let msg = msg.write_to_bytes().unwrap();
                    /*                    for v in msg.iter() {
                                            eprint!("{}, ", v);
                                        }*/
                    stdout.write_all(&split_number(msg.len()))?;
                    stdout.write_all(&msg)?;
                }
            }

            stdout.flush()?;
        }
        Ok(())
    });


    // Initialize Gstreamer
    gst::init().unwrap();

    let p = player::Player::new(1024);

    p.play(|spec| {
        if let Ok(s) = r_requests.try_recv() {
            match s {
                ServerRequest_Type::GET_AUDIO_SPECTRUM => {
                    let mut msg = audio_spectrum::AudioSpectrum::new();
                    msg.set_num_bands(spec.len() as i32);
                    msg.set_bands(spec);
                    t_spectrum.send(msg).unwrap();
                }
            }
        }
    });
}

fn split_number(value: usize) -> Vec<u8> {
    let mut result: Vec<u8> = Vec::with_capacity(value / 255 + 1);
    let mut value = value;

    while value > 255 {
        result.push(255);
        value -= 255;
    }
    result.push(value as u8);

    result
}