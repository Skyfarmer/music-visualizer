extern crate gstreamer as gst;

use gst::prelude::*;

pub struct Player {
    pipeline: gst::Pipeline,
    bands: u32,
}

impl Player {
    //TODO Change to i32
    pub fn new(bands: u32) -> Player {
        let source = gst::ElementFactory::make("filesrc", "source").expect("Could not create source element.");
        let decoder = gst::ElementFactory::make("decodebin", "decoder").expect("Could not create decode element.");
        let audioconverter = gst::ElementFactory::make("audioconvert", "audioconverter").expect("Could not create audioconvert element.");
        let spectrum = gst::ElementFactory::make("spectrum", "spectrum").expect("Could not create spectrum element.");
        let sink = gst::ElementFactory::make("autoaudiosink", "sink").expect("Could not create sink element.");

        let pipeline = gst::Pipeline::new("main-pipeline");

        spectrum.set_property_from_str("bands", &bands.to_string());
        spectrum.set_property_from_str("threshold", "-100");
        spectrum.set_property_from_str("post-messages", "TRUE");
        spectrum.set_property_from_str("interval", "1000000");


        pipeline.add_many(&[&source, &decoder, &audioconverter, &spectrum, &sink]).unwrap();
        source.link(&decoder).expect("Source could not be linked with decoder");
        audioconverter.link(&spectrum).expect("Converter could not be linked with spectrum");
        spectrum.link(&sink).expect("Spectrum could not be linked with sink");

        let converter_sink_pad = audioconverter.get_static_pad("sink").expect("Failed to get static sink pad from audioconverter");
        decoder.connect_pad_added(move |_, src_pad| {
            if converter_sink_pad.is_linked() {
                eprintln!("Already linked!");
                return;
            }

            let res = src_pad.link(&converter_sink_pad);
            if !res.is_ok() {
                eprintln!("Link failed");
            } else {
                eprintln!("Link succeeded.");
            }
        });

        source.set_property_from_str("location", "/home/shimmelbauer/Music/t.mp3");

        Player { pipeline, bands }
    }

    pub fn play<T>(&self, spectrum_function: T) where T: Fn(Vec<f32>) {
        self.pipeline.set_state(gst::State::Playing).expect("Unable to set the pipeline to playing state");

        let bus = self.pipeline.get_bus().unwrap();
        for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
            use gst::MessageView;
            match msg.view() {
                MessageView::Error(err) => {
                    eprintln!("Error received from element {:?}: {}",
                              err.get_src().map(|s| s.get_path_string()),
                              err.get_error());
                    eprintln!("Debugging information: {:?}", err.get_debug());
                    break;
                }
                MessageView::Element(e) => {
                    let msg_structure = e.get_structure().expect("Could not fetch structure.");
                    let msg_name = msg_structure.get_name();
                    if msg_name.starts_with("spectrum") {
                        let magnitudes = msg_structure.get_value("magnitude").unwrap();
                        let magnitudes: gst::List = magnitudes.get().unwrap();
                        spectrum_function(magnitudes.as_slice()
                            .iter()
                            .map(|x| x.get().unwrap())
                            .collect());
                    }
                }
                MessageView::Eos(..) => break,
                _ => (),
            }
        }

        self.pipeline.set_state(gst::State::Null).expect("Unable to set pipeline to NULL");
    }

    pub fn get_bands(&self) -> u32 {
        self.bands
    }
}
