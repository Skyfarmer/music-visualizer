// This file is generated by rust-protobuf 2.4.0. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq, Clone, Default)]
pub struct ServerRequest {
    // message fields
    pub field_type: ServerRequest_Type,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl ServerRequest {
    pub fn new() -> ServerRequest {
        ::std::default::Default::default()
    }

    // .ServerRequest.Type type = 1;

    pub fn clear_field_type(&mut self) {
        self.field_type = ServerRequest_Type::GET_AUDIO_SPECTRUM;
    }

    // Param is passed by value, moved
    pub fn set_field_type(&mut self, v: ServerRequest_Type) {
        self.field_type = v;
    }

    pub fn get_field_type(&self) -> ServerRequest_Type {
        self.field_type
    }
}

impl ::protobuf::Message for ServerRequest {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.field_type, 1, &mut self.unknown_fields)?
                }
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                }
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if self.field_type != ServerRequest_Type::GET_AUDIO_SPECTRUM {
            my_size += ::protobuf::rt::enum_size(1, self.field_type);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if self.field_type != ServerRequest_Type::GET_AUDIO_SPECTRUM {
            os.write_enum(1, self.field_type.value())?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> ServerRequest {
        ServerRequest::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<ServerRequest_Type>>(
                    "type",
                    |m: &ServerRequest| { &m.field_type },
                    |m: &mut ServerRequest| { &mut m.field_type },
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ServerRequest>(
                    "ServerRequest",
                    fields,
                    file_descriptor_proto(),
                )
            })
        }
    }

    fn default_instance() -> &'static ServerRequest {
        static mut instance: ::protobuf::lazy::Lazy<ServerRequest> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ServerRequest,
        };
        unsafe {
            instance.get(ServerRequest::new)
        }
    }
}

impl ::protobuf::Clear for ServerRequest {
    fn clear(&mut self) {
        self.clear_field_type();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ServerRequest {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ServerRequest {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum ServerRequest_Type {
    GET_AUDIO_SPECTRUM = 0,
}

impl ::protobuf::ProtobufEnum for ServerRequest_Type {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<ServerRequest_Type> {
        match value {
            0 => ::std::option::Option::Some(ServerRequest_Type::GET_AUDIO_SPECTRUM),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [ServerRequest_Type] = &[
            ServerRequest_Type::GET_AUDIO_SPECTRUM,
        ];
        values
    }

    fn enum_descriptor_static() -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("ServerRequest_Type", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for ServerRequest_Type {}

impl ::std::default::Default for ServerRequest_Type {
    fn default() -> Self {
        ServerRequest_Type::GET_AUDIO_SPECTRUM
    }
}

impl ::protobuf::reflect::ProtobufValue for ServerRequest_Type {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x14serverRequests.proto\"X\n\rServerRequest\x12'\n\x04type\x18\x01\
    \x20\x01(\x0e2\x13.ServerRequest.TypeR\x04type\"\x1e\n\x04Type\x12\x16\n\
    \x12GET_AUDIO_SPECTRUM\x10\0J\xbb\x01\n\x06\x12\x04\0\0\x08\x01\n\x08\n\
    \x01\x0c\x12\x03\0\0\x12\n\n\n\x02\x04\0\x12\x04\x02\0\x08\x01\n\n\n\x03\
    \x04\0\x01\x12\x03\x02\x08\x15\n\x0c\n\x04\x04\0\x04\0\x12\x04\x03\x08\
    \x05\t\n\x0c\n\x05\x04\0\x04\0\x01\x12\x03\x03\r\x11\n\r\n\x06\x04\0\x04\
    \0\x02\0\x12\x03\x04\x10'\n\x0e\n\x07\x04\0\x04\0\x02\0\x01\x12\x03\x04\
    \x10\"\n\x0e\n\x07\x04\0\x04\0\x02\0\x02\x12\x03\x04%&\n\x0b\n\x04\x04\0\
    \x02\0\x12\x03\x07\x08\x16\n\r\n\x05\x04\0\x02\0\x04\x12\x04\x07\x08\x05\
    \t\n\x0c\n\x05\x04\0\x02\0\x06\x12\x03\x07\x08\x0c\n\x0c\n\x05\x04\0\x02\
    \0\x01\x12\x03\x07\r\x11\n\x0c\n\x05\x04\0\x02\0\x03\x12\x03\x07\x14\x15\
    b\x06proto3\
";

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
